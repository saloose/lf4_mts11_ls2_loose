import java.util.ArrayList;

/**
 * @author Simon Loose
 *
 */
public class Raumschiff {

	private String schiffsname;
	private int energieversorgung;
	private int schutzschilde;
	private int lebenserhaltungssysteme;
	private int huelle;
	private int photonentorpedos;
	private int reparaturAndroiden;
	private ArrayList<Ladung> ladungsverzeichnis;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<String> logbuch;
	private Kapitaen kapitaen;
	
	/**
	 * Erstellt eine Instanz der Klasse Raumschiff
	 * Alle Attribute werden mit leeren Werten bzw. 0 initialisiert
	 */
	public Raumschiff() {
		setSchiffsname("");
		setEnergieversorgung(0);
		setSchutzschilde(0);
		setLebenserhaltungssysteme(0);
		setHuelle(0);
		setPhotonentorpedos(0);
		setReparaturAndroiden(0);
		setLadungsverzeichnis(new ArrayList<Ladung>());
		setLogbuch(new ArrayList<String>());
		setKapitaen(new Kapitaen());
	}
	
	/**
	 * Erstellt eine Instanz der Klasse Raumschiff
	 * @param schiffsname Der Name des Schiffs
	 * @param energieversorgung Der Zustand der Energieversorgung in %
	 * @param schutzschilde Der Zustand der Schutzschilde in %
	 * @param lebenserhaltungssysteme Der Zustand der Lebenserhaltungssysteme in % 
	 * @param huelle Der Zustand der Huelle in %
	 * @param photonentorpedos Die Anzahl an Photonentorpedos
	 * @param reparaturAndroiden Die Anzahl an Reparaturandroiden
	 * @param ladungsverzeichnis Das Ladungsverzeichnis
	 * @param logbuch Das Logbuch
	 * @param kapitaen Der Kapitaen des Schiffs
	 */
	public Raumschiff(String schiffsname, int energieversorgung, int schutzschilde,
			int lebenserhaltungssysteme, int huelle, int photonentorpedos, int reparaturAndroiden,
			ArrayList<Ladung> ladungsverzeichnis, ArrayList<String> logbuch, Kapitaen kapitaen) {

		setSchiffsname(schiffsname);
		setEnergieversorgung(energieversorgung);
		setSchutzschilde(schutzschilde);
		setLebenserhaltungssysteme(lebenserhaltungssysteme);
		setHuelle(huelle);
		setPhotonentorpedos(photonentorpedos);		
		setReparaturAndroiden(reparaturAndroiden);
		setLadungsverzeichnis(ladungsverzeichnis);
		setLogbuch(logbuch);
		setKapitaen(kapitaen);
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public int getEnergieversorgung() {
		return energieversorgung;
	}

	/**
	 * Setzt die Energieversorgung (in %). Setzt den Wert auf 0, falls ein negativer Wert �bergeben wird.
	 * @param energieversorgung 
	 */
	public void setEnergieversorgung(int energieversorgung) {
		if(energieversorgung < 0) {
			this.energieversorgung = 0;
		} else {
			this.energieversorgung = energieversorgung;
		}
	}

	public int getSchutzschilde() {
		return schutzschilde;
	}

	/**
	 * Setzt die Schutzschilde (in %). Setzt den Wert auf 0, falls ein negativer Wert �bergeben wird.
	 * @param schutzschilde 
	 */
	public void setSchutzschilde(int schutzschilde) {
		if(schutzschilde < 0) {
			this.schutzschilde = 0;
		}else {
			this.schutzschilde = schutzschilde;
		}
	}

	public int getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}

	/**
	 * Setzt die Lebenserhaltungssysteme (in %). Setzt den Wert auf 0, falls ein negativer Wert �bergeben wird.
	 * @param lebenserhaltungssysteme 
	 */
	public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
		if(lebenserhaltungssysteme < 0) {
			this.lebenserhaltungssysteme = 0;
		}else {
			this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		}
	}

	public int getHuelle() {
		return huelle;
	}

	/**
	 * Setzt die Huelle (in %). Setzt den Wert auf 0, falls ein negativer Wert �bergeben wird.
	 * @param huelle 
	 */
	public void setHuelle(int huelle) {
		if(huelle < 0) {
			this.huelle = 0;
		}else {
			this.huelle = huelle;
		}
	}

	public int getPhotonentorpedos() {
		return photonentorpedos;
	}

	/**
	 * @param photonentorpedos will be set to 0 if negative
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		if(photonentorpedos < 0) {
			this.photonentorpedos = 0;
		}else {
			this.photonentorpedos = photonentorpedos;
		}
	}

	public int getReparaturAndroiden() {
		return reparaturAndroiden;
	}

	/**
	 * @param reparaturAndroiden will be set to 0 if negative
	 */
	public void setReparaturAndroiden(int reparaturAndroiden) {
		if(reparaturAndroiden < 0) {
			this.reparaturAndroiden = 0;
		}else {
			this.reparaturAndroiden = reparaturAndroiden;
		}
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public static ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<String> getLogbuch() {
		return logbuch;
	}

	public void setLogbuch(ArrayList<String> logbuch) {
		this.logbuch = logbuch;
	}

	public Kapitaen getKapitaen() {
		return kapitaen;
	}

	public void setKapitaen(Kapitaen kapitaen) {
		this.kapitaen = kapitaen;
	}
	
	/**
	 * F�gt die �bergebene Ladung dem Schiff hinzu. Wenn schon eine Ladung dieses Typs geladen ist, wird die Menge erh�ht.
	 * @param ladung Die zu ladende Ladung
	 */
	public void addLadung(Ladung ladung) {
		for(Ladung l : ladungsverzeichnis) {
			if(l.getTyp().equalsIgnoreCase(ladung.getTyp())) {
				l.setMenge(l.getMenge()+ladung.getMenge());
				return;
			}
		}
		ladungsverzeichnis.add(ladung);
	}
	
	/**
	 * Schiesst einen Photonentorpedo auf das Ziel, falls Torpedos geladen sind. Die Anzahl an Torpedos verringert sich dadurch um 1.
	 * Es wird eine Nachricht an alle Schiffe gesendet mit -=*Click*=- falls keine Torpedos vorhanden waren oder 
	 * Photonentorpedo abgeschossen falls Torpedos vorhanden waren
	 * @param raumschiff Das Ziel des Angriffs
	 */
	public void photonentorpedosSchiessen(Raumschiff raumschiff) {
		if(getPhotonentorpedos() == 0) {
			broadcast("-=*Click*=-");
		} else {
			setPhotonentorpedos(getPhotonentorpedos() - 1);
			broadcast("Photonentorpedo abgeschossen");
			raumschiff.treffer(this);
		}
	}
	
	/**
	 * Schiesst die Phaserkanonen auf das Ziel, falls mindestens 50% Energie vorhanden ist und reduziert danach die Energieversorgung um 50%.
	 * Es wird eine Nachricht an alle Schiffe gesendet mit -=*Click*=- falls nicht genug Energie vorhanden war oder
	 * Phaserkanone abgeschossen falls genug Energie vorhanden war.
	 * @param raumschiff Das Ziel des Angriffs
	 */
	public void phaserkanonenSchiessen(Raumschiff raumschiff) {
		if(getEnergieversorgung() < 50) {
			broadcast("-=*Click*=-");
		}else {
			setEnergieversorgung(getEnergieversorgung() - 50);
			broadcast("Phaserkanone abgeschossen");
			raumschiff.treffer(this);
		}
	}
	
	/**
	 * Gibt eine Meldung auf der Konsole aus, dass das Schiff getroffen wurde.
	 * Reduziert die Schilde um 50%. Falls die Schilde auf 0% fallen werden Huelle und Energieversorgung um 50% reduziert.
	 * Falls die Huelle dadurch auf 0% f�llt werden die Lebenserhaltungssysteme zerst�rt und dies an alle Raumschiffe gemeldet.
	 * @param angreifer Das angreifende Raumschiff
	 */
	private void treffer(Raumschiff angreifer) {
		System.out.println(schiffsname + " wurde getroffen!");
		setSchutzschilde(getSchutzschilde() - 50);
		if(getSchutzschilde() == 0) {
			setHuelle(getHuelle() - 50);
			setEnergieversorgung(getEnergieversorgung() - 50);
			if(getHuelle() == 0) {
				setLebenserhaltungssysteme(0);
				broadcast("Lebenserhaltungssysteme wurden vernichtet");
			}
		}
	}
	
	/**
	 * Sendet eine Nachricht an alle Raumschiffe
	 * @param nachricht die zu sendende Nachricht
	 */
	public void broadcast(String nachricht) {
		broadcastKommunikator.add(nachricht);
	}
	
	/**
	 * @return
	 */
	public static ArrayList<String> logbuchAusgeben(){
		return broadcastKommunikator;
	}
	
	/**
	 * L�dt Photonentorpedos die das Raumschiff in seinem Ladungsverzeichnis stehen hat in die Torpedorohre, sodass diese verwendet werden k�nnen.
	 * Falls keine Photonentorpedos im Ladungsverzeichnis aufgef�hrt waren, wird dies auf der Konsole ausgegeben und ein -=*Click*=- an alle Raumschiffe gesendet.
	 * Falls Photonentorpedos im Ladungsverzeichnis aufgef�hrt waren, werden diese geladen und aus dem Ladungsverzeichnis entfernt.
	 * Danach wird eine Nachricht auf der Konsole ausgegeben, die angibt wie viele Torpedos geladen wurden.
	 * @param anzahl Die maximale Anzahl an Torpedos die geladen werden sollen.
	 */
	public void torpedoRohreLaden(int anzahl) {
		for(Ladung l : ladungsverzeichnis) {
			if(l.getTyp().equalsIgnoreCase("Photonentorpedo")) {
				if(l.getMenge() >= anzahl) {
					l.setMenge(l.getMenge() - anzahl);
					setPhotonentorpedos(getPhotonentorpedos() + anzahl);
					System.out.println("" + anzahl  + " Photonentorpedo(s) eingesetzt");
				} else {
					setPhotonentorpedos(getPhotonentorpedos() + l.getMenge());
					System.out.println("" + l.getMenge()  + " Photonentorpedo(s) eingesetzt");
					l.setMenge(0);
				}
				return;
			}
		}
		System.out.println("Keine Photonentorpedos gefunden!");
		broadcast("-=*Click*=-");
	}
	
	/**
	 * Repariert die angegebenen Komponenten um einen zuf�lligen Wert.
	 * Der Wert ist eine Zahl zwischen 0 und 100 multipliziert mit der Anzahl an verwendeten Androiden geteilt durch die Anzahl der zu reparierenden Komponenten. 
	 * @param schutzschilde Gibt an ob die Schutzschilde repariert werden sollen.
	 * @param energieversorgung Gibt an ob die Energieversorgung repariert werden soll.
	 * @param schiffshuelle Gibt an ob die Huelle repariert werden soll.
	 * @param androiden Die Anzahl an Reparaturandroiden, die verwendet werden sollen. Wenn weniger Androiden vorhanden sind als angegeben, werden alle verwendet.
	 */
	public void reparaturBeauftragen(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int androiden) {
		int droiden = Math.max(0, Math.min(androiden, reparaturAndroiden));
		int repwert = (int) (100 * Math.random());
		int strukturen = 0;
		if(schutzschilde) {
			strukturen++;
		}
		if(energieversorgung) {
			strukturen++;
		}
		if(schiffshuelle) {
			strukturen++;
		}
		repwert = repwert * droiden / strukturen;
		if(energieversorgung) {
			setEnergieversorgung(this.getEnergieversorgung() + repwert);
		}
		if(schutzschilde) {
			setSchutzschilde(this.getSchutzschilde() + repwert);
		}
		if(schiffshuelle) {
			setHuelle(this.getHuelle() + repwert);
		}
	}
	
	/**
	 * Gibt den Name, den Zustand der Energieversorgung, Huelle, Schilde und Lebenserhaltungssysteme, sowie die Anzahl an Photonentorpedos und Reparaturandroiden aus.
	 */
	public void zustandAnzeigen() {
		System.out.println("Name: " + schiffsname);
		System.out.println("Energieversorgung: " + energieversorgung);
		System.out.println("Huelle: " + huelle);
		System.out.println("Schilde " + schutzschilde);
		System.out.println("Lebenserhaltungssysteme: " + lebenserhaltungssysteme);
		System.out.println("Photonentorpedos: " + photonentorpedos);
		System.out.println("Reparaturandroiden: " + reparaturAndroiden);
	}
	
	/**
	 * Gibt das Ladungsverzeichnis aus.
	 */
	public void ladungsverzeichnisAusgeben() {
		for(Ladung l : ladungsverzeichnis) {
			System.out.println("" + l.getMenge() + " " + l.getTyp());
		}
	}
	
	/**
	 * Entfernt alle Eintr�ge mit Menge 0 aus dem Ladungsverzeichnis.
	 */
	public void ladungsverzeichnisAufraeumen() {
		boolean b = true;
		while(b) {
			for(Ladung l : ladungsverzeichnis) {
				if(l.getMenge() == 0) {
					ladungsverzeichnis.remove(l);
					break;
				}
			}
		}
		
	}
}
