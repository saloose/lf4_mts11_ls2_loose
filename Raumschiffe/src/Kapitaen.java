
public class Kapitaen {

	private String name;
	private int dienstbeginn;
	
	public Kapitaen() {
	}

	public Kapitaen(String name, int dienstbeginn) {
		this.name = name;
		this.dienstbeginn = dienstbeginn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDienstbeginn() {
		return dienstbeginn;
	}

	public void setDienstbeginn(int dienstbeginn) {
		this.dienstbeginn = dienstbeginn;
	}
		
}
