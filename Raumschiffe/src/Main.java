import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2, new ArrayList<Ladung>(), new ArrayList<String>(), null);
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert",200));
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft",200));
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2, new ArrayList<Ladung>(), new ArrayList<String>(), null);
		romulaner.addLadung(new Ladung("Plasma-Waffe",50));
		romulaner.addLadung(new Ladung("Rote Materie",2));
		romulaner.addLadung(new Ladung("Borg-Schrott",5));
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5, new ArrayList<Ladung>(), new ArrayList<String>(), null);
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));
		klingonen.photonentorpedosSchiessen(romulaner);
		romulaner.phaserkanonenSchiessen(klingonen);
		vulkanier.broadcast("Gewalt ist nicht logisch");
		klingonen.zustandAnzeigen();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reparaturBeauftragen(true, true, true, 5);
		vulkanier.torpedoRohreLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedosSchiessen(romulaner);
		klingonen.photonentorpedosSchiessen(romulaner);
		klingonen.zustandAnzeigen();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandAnzeigen();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandAnzeigen();
		vulkanier.ladungsverzeichnisAusgeben();
		for(String s: Raumschiff.getBroadcastKommunikator()) {
			System.out.println(s);
		}
	}

}
