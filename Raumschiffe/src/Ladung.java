
/**
 * @author Simon Loose
 *
 */
public class Ladung {
	
	private String typ;
	private int menge;
		
	/**
	 * Erstellt ein Objekt der Klasse Ladung.
	 * Als Ladungstyp wird ein leerer String initialisiert und als Menge 0.
	 */
	public Ladung() {
		typ = "";
		menge = 0;
	}

	/**
	 * Erstellt ein Objekt der Klasse Ladung.
	 * @param typ Der Typ der Ladung 
	 * @param menge Die Menge an Ladung
	 */
	public Ladung(String typ, int menge) {
		this.typ = typ;
		this.menge = menge;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public int getMenge() {
		return menge;
	}

	/**
	 * Setzt die Ladungsmenge. Wird bei negativen Werten auf 0 gesetzt.
	 * @param menge Die Menge der Ladung
	 */
	public void setMenge(int menge) {
		if(menge < 0) {
			this.menge = 0;
		} else {
			this.menge = menge;

		}
	}


	@Override
	public String toString() {
		return "Ladung - Typ: " + this.typ + " - Anzahl: " + menge;
	}
	
		
}
